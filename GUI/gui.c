#include "gui.h"
#include "lcd_init.h"
#include "lcd.h"
#include "string.h"
#include "adc.h"
#include "app.h"
#include "pwm.h"
#include <stdint.h>
#define FONT_W 12
#define FONT_H 26
#define FONT_SIZE 24
typedef struct 
{
    /* data */
    char date[12];
    char version[8];
}version_t;
version_t test_pen_version = {
    __DATE__,"V0.0.01"
};
void main_window(uint32_t key_sta);
void main_control(uint32_t key_sta);
void ttl_window(uint32_t key_sta);
void ttl_control(uint32_t key_sta);
void pwm_window(uint32_t key_sta);
void pwm_control(uint32_t key_sta);
void dc_window(uint32_t key_sta);
void dc_control(uint32_t key_sta);
void onoff_window(uint32_t key_sta);
void onoff_control(uint32_t key_sta);
void diode_window(uint32_t key_sta);
void diode_control(uint32_t key_sta);
void adj_window(uint32_t key_sta);
void adj_control(uint32_t key_sta);
// main menu
menu_t main_menu[7] = {
    {"TTL", 3 * FONT_W, ttl_window, ttl_control},
    {"PWM", 3 * FONT_W, pwm_window, pwm_control},
    {"DC OUT", 6 * FONT_W, dc_window, dc_control},
    {"On/Off", 6 * FONT_W, onoff_window, onoff_control},
    {"Diode", 5 * FONT_W, diode_window, diode_control},
    {"Calibration", 11 * FONT_W, adj_window, adj_control},
    // {"Other",5*FONT_W,}
};
window_t main_Win = {
    0,
    0,
    6,
    main_menu,
    NULL,
};
// sub win
menu_t ttl_menu[2] = {
    {"Hight", 40, NULL},
    {"L o w", 24, NULL},
};
param_t ttl_param[2] = {
    {2000, 18000, 0, 100},
    {1000, 2000, 0, 100},
};
window_t ttl_Win = {
    0,
    0,
    2,
    ttl_menu,
    ttl_param,
};
// pwm win
menu_t pwm_menu[3] = {
    {"Duty", 40, NULL},
    {"KHz", 36, NULL},
    {"Hz", 24, NULL},
};
param_t pwm_param[3] = {
    {50, 100, 0, 1},
    {1, 100, 0, 1},
    {500, 1000, 0, 1},
};
window_t pwm_Win = {
    0,
    0,
    3,
    pwm_menu,
    pwm_param,
};
// dcout win
menu_t dc_menu[2] = {
    {"DC", 40, NULL},
};
param_t dc_param[1] = {
    {2000, 5800, 600, 100},
};
window_t dc_Win = {
    0,
    0,
    1,
    dc_menu,
    dc_param,
};
// onoff win
menu_t onoff_menu[2] = {
    {"R", 40, NULL},
};
param_t onoff_param[1] = {
    {100, 200, 0, 1},
};
window_t onoff_Win = {
    0,
    0,
    1,
    onoff_menu,
    onoff_param,
};
// diode win
menu_t diode_menu[1] = {
    {"diode", 5 * FONT_W, NULL},
};
window_t diode_Win = {
    0,
    0,
    1,
    diode_menu,
    NULL,
};
// adj win
menu_t adj_menu[2] = {
    {"adj", 40, NULL},
};
window_t adj_Win = {
    0,
    0,
    1,
    adj_menu,
    NULL,
};
//
window_func_t run_window = NULL;
window_func_t run_control = NULL;
window_t *win_buf = NULL;
uint8_t win_init = 0;
uint8_t select_flag = 1;

int dis_num = 3;   // 最大显示条数
int ui_select = 0; // 当前选择菜单序号

short cursor_x = 8; // 当前坐标x值 2是矩形的x坐标 距离最左边2  字体会在加4后开始绘制
short cursor_y = 2; // 当前坐标y值  文字的坐标是左下角参考点  第一个字 是 0,15
uint16_t bc_color[3] = {0xFF30, N_Coloe_B16LightSkyBlue, N_Coloe_B16LightPink};
uint16_t set_light(uint16_t color, uint8_t light)
{
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    red = (color & 0xf800) >> 11;
    green = (color & 0x07e0) >> 5;
    blue = (color & 0x001f);

    red = red * light / 10;
    green = red * light / 10;
    blue = red * light / 10;

    red = red > 0x1f ? 0x1f : red;
    green = green > 0x3f ? 0x3f : green;
    blue = blue > 0x1f ? 0x1f : blue;

    return (red << 11) | (green << 5) | (blue);
}
void gui_config(void)
{
    LCD_Init();
    LCD_Fill(0, 0, LCD_W, LCD_H, BLACK);

    run_window = main_window;
    run_control = main_control;
}
char showbuf[20] = {0};
void show_pen_volt(uint16_t x, uint16_t y, uint16_t volt, uint16_t fc, uint16_t bc)
{
    uint16_t vt;
    uint16_t mvt;
    vt = (volt / 1000) % 10;
    mvt = (volt % 1000) / 10;
    sprintf(showbuf, "%.1d.%.2dV\0", vt, mvt);
    LCD_ShowString(x, y, showbuf, fc, bc, FONT_SIZE, 0); // RED, BLACK, FONT_SIZE
}
void show_bat_percent(void)
{
    uint16_t bat_val;
    uint16_t percent = 0;
    bat_val = get_bat_val();
    // 先画电池框
    uint8_t bat_box_x = 142;
    uint8_t bat_box_hight = 7;
    LCD_DrawLine(bat_box_x, 2, bat_box_x, bat_box_hight - 2, BLUE);
    LCD_DrawLine(bat_box_x + 1, 2, bat_box_x + 1, bat_box_hight - 2, BLUE);
    LCD_DrawRectangle(bat_box_x + 2, 0, 160, bat_box_hight, BLUE);
    // 画电量格数
    uint8_t w = 4;        // 显示格子宽
    uint8_t interval = 1; // 格子间的间隔
    uint8_t h = 6;        // 格子的高
    uint8_t y = 1;
    uint8_t y2 = y + h;
    uint8_t x1 = bat_box_x + 3;
    uint8_t x2 = x1 + w + interval;
    uint8_t x3 = x2 + w + interval;
    if (bat_val > 3900)
    {
        LCD_Fill(x1, y, x1 + w, y2, GREEN);
        LCD_Fill(x2, y, x2 + w, y2, GREEN);
        LCD_Fill(x3, y, x3 + w, y2, GREEN);
    }
    else if (bat_val > 3700)
    {
        LCD_Fill(x1, y, x1 + w, y2, bc_color[0]);
        LCD_Fill(x2, y, x2 + w, y2, GREEN);
        LCD_Fill(x3, y, x3 + w, y2, GREEN);
    }
    else if (bat_val > 3500)
    {
        LCD_Fill(x1, y, x2 + w, y2, bc_color[0]);
        LCD_Fill(x3, y, x3 + w, y2, RED);
    }
    else
    {
        LCD_Fill(x1, y, x3 + w, y2, bc_color[0]);
    }
}
void show_pen_gears(void)
{
    char buf[5];
    sprintf(buf, "x%d \0", pen_gears);
    LCD_ShowString(0, 0, buf, RED, bc_color[0], 12, 0);
}
void clear_window(void)
{
    LCD_Fill(0, 0, LCD_W, 27, bc_color[0]);
    LCD_Fill(0, 27, LCD_W, 27 + 26, bc_color[1]);
    LCD_Fill(0, 53, LCD_W, 80, bc_color[2]);
}
void select_item_color(uint8_t item_id)
{
    if (select_flag == 1)
    {
        select_flag = 0;
        switch (item_id)
        {
        case 0:
            LCD_Fill(0, 0, LCD_W, 27, BLACK);
            LCD_Fill(0, 27, LCD_W, 27 + 26, bc_color[1]);
            LCD_Fill(0, 53, LCD_W, 80, bc_color[2]);
            break;
        case 1:
            LCD_Fill(0, 0, LCD_W, 27, bc_color[0]);
            LCD_Fill(0, 27, LCD_W, 27 + 26, BLACK);
            LCD_Fill(0, 53, LCD_W, 80, bc_color[2]);
            break;
        case 2:
            LCD_Fill(0, 0, LCD_W, 27, bc_color[0]);
            LCD_Fill(0, 27, LCD_W, 27 + 26, bc_color[1]);
            LCD_Fill(0, 53, LCD_W, 80, BLACK);
            break;
        default:
            LCD_Fill(0, 0, LCD_W, 27, bc_color[0]);
            LCD_Fill(0, 27, LCD_W, 27 + 26, bc_color[1]);
            LCD_Fill(0, 53, LCD_W, 80, bc_color[2]);
            break;
        }
    }
}
void main_window(uint32_t key_sta)
{
    if (win_init == 0)
    {
        logical_level_mode();
        win_init = 1;
        win_buf = &main_Win;
        clear_window();
    }

    int tem = win_buf->menu_id - dis_num + 1;
    if (tem <= 0)
    {
        tem = 0;
    }
    for (int i = 0; i < dis_num; i++)
    {
        if (win_buf->menu_id == (tem + i))
        {
            select_item_color(i);
            LCD_ShowString(80 - win_buf->menuList[tem + i].len / 2, cursor_y + i * FONT_H, win_buf->menuList[tem + i].str, bc_color[i], BLACK, FONT_SIZE, 0);
        }
        else
        {
            LCD_ShowString(80 - win_buf->menuList[tem + i].len / 2, cursor_y + i * FONT_H, win_buf->menuList[tem + i].str, BLACK, bc_color[i], FONT_SIZE, 0);
        }
    }
    show_bat_percent();
}
void main_control(uint32_t key_sta)
{
    if (win_init == 0)
    {
        win_buf = &main_Win;
    }
    switch (key_sta)
    {
    case KEY_UP:
        if (win_buf->menu_id == 0)
        {
            win_buf->menu_id = win_buf->list_len; // 定位到最后一条，从0开始数的
        }
        win_buf->menu_id--;
        select_flag = 1;
        break;
    case KEY_DOW:
        win_buf->menu_id++;
        if (win_buf->menu_id >= win_buf->list_len)
        {
            win_buf->menu_id = 0;
        }
        select_flag = 1;
        break;
    case KEY_LEFT:
        // 开关照明灯
        toggle_led_light();
        break;
    case KEY_RIGHT:
        // 下一级窗口
        win_init = 0;
        run_window = win_buf->menuList[win_buf->menu_id].sub_func;
        run_control = win_buf->menuList[win_buf->menu_id].control_func;
        select_flag = 1;
        break;
    case KEY_OK:

        break;
    default:
        break;
    }
}
void back_to_main(void)
{
    win_init = 0;
    run_window = main_window;
    run_control = main_control;
    select_flag = 1;
}
void num_flash(uint16_t x, uint16_t y, uint16_t num, u8 len, uint16_t fc, uint16_t bc)
{
    static uint8_t sta;
    sta++;
    if (sta >= 6)
    {
        sta = 0;
    }
    if (sta > 2)
    {
        LCD_ShowIntNum(x, y, num, len, bc, bc, FONT_SIZE);
    }
    else
    {
        LCD_ShowIntNum(x, y, num, len, fc, bc, FONT_SIZE);
    }
}
void ttl_volte_to_ble(uint16_t vlote)
{
    static uint16_t cnt = 0;
    cnt++;
    if (cnt >= 10)
    {
        cnt = 0;
        printf("vlote:%u\n", vlote);
    }
}
void ttl_window(uint32_t key_sta)
{
    uint16_t pen_val;
    uint16_t bc;
    uint16_t fc = BLACK;
    if (win_init == 0)
    {
        logical_level_mode();
        win_init = 1;
        win_buf = &ttl_Win;
        clear_window();
    }
    pen_val = get_pen_val();
    for (int i = 0; i < win_buf->list_len; i++)
    {
        if (win_buf->menu_id == i)
        {
            fc = bc_color[i + 1];
            bc = BLACK;
            select_item_color(i + 1);
            if (win_buf->mode == 1)
            {
                num_flash(100, cursor_y + (i+1)*FONT_H, win_buf->paramList[i].val, 5, fc, bc);
            }
        }
        else
        {
            fc = BLACK;
            bc = bc_color[i + 1];
        }
        LCD_ShowString(cursor_x, cursor_y + (i + 1) * FONT_H, win_buf->menuList[i].str, fc, bc, FONT_SIZE, 0);
        LCD_ShowIntNum(100, cursor_y + (i + 1) * FONT_H, win_buf->paramList[i].val, 5, fc, bc, FONT_SIZE);
    }
    LCD_ShowIntNum(80 - 3 * 12, cursor_y, pen_val, 5, BLACK, bc_color[0], FONT_SIZE);
    LCD_ShowString(80 + 2 * 12, cursor_y, "mV", BLACK, bc_color[0], FONT_SIZE, 0);
    show_bat_percent();
    show_pen_gears();
    ttl_volte_to_ble(pen_val);
    // 高电平判断
    if (pen_val >= win_buf->paramList[0].val)
    {
        set_led_green(1);
        set_led_red(0);
    }
    else if (pen_val <= win_buf->paramList[1].val) // 低电平判断
    {
        set_led_red(1);
        set_led_green(0);
    }
    else
    {
        set_led_red(1);
        set_led_green(1);
    }
}
void ttl_control(uint32_t key_sta)
{
    if (win_init == 0)
    {
        win_buf = &ttl_Win;
    }
    switch (key_sta)
    {
    case KEY_UP:
        if (win_buf->mode == 0)
        {
            if (win_buf->menu_id > 0)
                win_buf->menu_id--;
            select_flag = 1;
        }
        else
        {
            if (win_buf->paramList[win_buf->menu_id].val <
                win_buf->paramList[win_buf->menu_id].max)
            {
                win_buf->paramList[win_buf->menu_id].val += win_buf->paramList[win_buf->menu_id].step;
            }
        }
        break;
    case KEY_DOW:
        if (win_buf->mode == 0)
        {
            win_buf->menu_id++;
            if (win_buf->menu_id >= win_buf->list_len)
            {
                win_buf->menu_id = 0;
            }
            select_flag = 1;
        }
        else
        {
            if (win_buf->paramList[win_buf->menu_id].val >
                win_buf->paramList[win_buf->menu_id].min)
            {
                win_buf->paramList[win_buf->menu_id].val -= win_buf->paramList[win_buf->menu_id].step;
            }
        }
        break;
    case KEY_LEFT:
        // 返回
        back_to_main();
        set_led_red(1);
        set_led_green(1);
        break;
    case KEY_RIGHT:
        win_buf->mode = win_buf->mode > 0 ? 0 : 1;
        break;
    case KEY_OK:

        break;
    default:
        break;
    }
}
void pwm_window(uint32_t key_sta)
{
    uint16_t bc;
    uint16_t fc = BLACK;
    if (win_init == 0)
    {
        pwm_out_mode();
        win_buf = &pwm_Win;
        win_init = 1;
        win_buf->paramList[0].val = get_pwm_duty();
        win_buf->paramList[1].val = get_pwm_hz() / 1000;
        win_buf->paramList[2].val = get_pwm_hz() % 1000;
        clear_window();
    }

    for (int i = 0; i < win_buf->list_len; i++)
    {
        if (win_buf->menu_id == i)
        {
            fc = bc_color[i];
            bc = BLACK;
            select_item_color(i);
            if (win_buf->mode == 1)
            {
                num_flash(60, cursor_y + (i)*FONT_H, win_buf->paramList[i].val, 4, fc, bc);
            }
        }
        else
        {
            fc = BLACK;
            bc = bc_color[i];
        }
        LCD_ShowString(cursor_x, cursor_y + (i)*FONT_H, win_buf->menuList[i].str, fc, bc, FONT_SIZE, 0);
        LCD_ShowIntNum(60, cursor_y + (i)*FONT_H, win_buf->paramList[i].val, 4, fc, bc, FONT_SIZE);
    }
    show_bat_percent();
}
void pwm_control(uint32_t key_sta)
{
    if (win_init == 0)
    {
        win_buf = &pwm_Win;
        win_buf->paramList[0].val = get_pwm_duty();
        win_buf->paramList[1].val = get_pwm_hz() / 1000;
        win_buf->paramList[2].val = get_pwm_hz() % 1000;
    }
    switch (key_sta)
    {
    case KEY_UP:
        if (win_buf->mode == 0)
        {
            if (win_buf->menu_id > 0)
                win_buf->menu_id--;
            select_flag = 1;
        }
        else
        {
            if (win_buf->paramList[win_buf->menu_id].val <
                win_buf->paramList[win_buf->menu_id].max)
            {
                win_buf->paramList[win_buf->menu_id].val += win_buf->paramList[win_buf->menu_id].step;
            }
        }
        break;
    case KEY_DOW:
        if (win_buf->mode == 0)
        {
            win_buf->menu_id++;
            if (win_buf->menu_id >= win_buf->list_len)
            {
                win_buf->menu_id = 0;
            }
            select_flag = 1;
        }
        else
        {
            if (win_buf->paramList[win_buf->menu_id].val >
                win_buf->paramList[win_buf->menu_id].min)
            {
                win_buf->paramList[win_buf->menu_id].val -= win_buf->paramList[win_buf->menu_id].step;
            }
        }
        break;
    case KEY_LEFT:
        // 返回
        back_to_main();
        break;
    case KEY_RIGHT:
        if (win_buf->mode == 1 && win_buf->menu_id == 0)
        {
            set_pwm_duty(win_buf->paramList[win_buf->menu_id].val);
        }
        if (win_buf->mode == 1 && win_buf->menu_id == 1)
        {
            set_pwm_hz(win_buf->paramList[1].val * 1000 + win_buf->paramList[2].val);
        }
        win_buf->mode = win_buf->mode > 0 ? 0 : 1;
        break;
    case KEY_OK:

        break;
    default:
        break;
    }
}
void dc_window(uint32_t key_sta)
{
    uint16_t bc;
    uint16_t fc = BLACK;
    if (win_init == 0)
    {
        dc_out_mode();
        win_buf = &dc_Win;
        win_init = 1;
        win_buf->paramList->val = get_dc_val();
        clear_window();
    }
    for (int i = 0; i < win_buf->list_len; i++)
    {
        if (win_buf->menu_id == i)
        {
            fc = bc_color[i + 1];
            bc = BLACK;
            select_item_color(i + 1);
            if (win_buf->mode == 1)
            {
                num_flash(60, cursor_y + (i + 1) * FONT_H, win_buf->paramList[i].val, 4, fc, bc);
            }
        }
        else
        {
            fc = BLACK;
            bc = bc_color[i + 1];
        }
        LCD_ShowString(cursor_x, cursor_y + (i + 1) * FONT_H, win_buf->menuList[i].str, fc, bc, FONT_SIZE, 0);
        LCD_ShowIntNum(60, cursor_y + (i + 1) * FONT_H, win_buf->paramList[i].val, 4, fc, bc, FONT_SIZE);

        LCD_ShowString(136, cursor_y + (i + 1) * FONT_H, "mV", fc, bc, FONT_SIZE, 0);
    }
    show_bat_percent();
}
void dc_control(uint32_t key_sta)
{
    if (win_init == 0)
    {
        win_buf = &dc_Win;
        win_buf->paramList->val = get_dc_val();
    }
    switch (key_sta)
    {
    case KEY_UP:
        if (win_buf->mode == 0)
        {
            if (win_buf->menu_id > 0)
                win_buf->menu_id--;
        }
        else
        {
            if (win_buf->paramList[win_buf->menu_id].val <
                win_buf->paramList[win_buf->menu_id].max)
            {
                win_buf->paramList[win_buf->menu_id].val += win_buf->paramList[win_buf->menu_id].step;
            }
        }
        break;
    case KEY_DOW:
        if (win_buf->mode == 0)
        {
            win_buf->menu_id++;
            if (win_buf->menu_id >= win_buf->list_len)
            {
                win_buf->menu_id = 0;
            }
        }
        else
        {
            if (win_buf->paramList[win_buf->menu_id].val >
                win_buf->paramList[win_buf->menu_id].min)
            {
                win_buf->paramList[win_buf->menu_id].val -= win_buf->paramList[win_buf->menu_id].step;
            }
        }
        break;
    case KEY_LEFT:
        // 返回
        back_to_main();
        break;
    case KEY_RIGHT:
        if (win_buf->mode == 1 && win_buf->menu_id == 0)
        {
            set_dc_val(win_buf->paramList[win_buf->menu_id].val);
        }
        win_buf->mode = win_buf->mode > 0 ? 0 : 1;
        break;
    case KEY_OK:
        break;
    default:
        break;
    }
}
const unsigned char oumu[16*21] = {/*--  文字:  Ω  --*/
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xF8,0x03,0x1C,0x07,0x0E,0x0E,0x0F,0x1E,
0x07,0x1E,0x07,0x1C,0x07,0x1E,0x0F,0x1E,0x0E,0x0E,0x1E,0x0F,0x9C,0x07,0xBF,0x1F,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
void onoff_window(uint32_t key_sta)
{
    uint16_t bc;
    uint16_t fc = BLACK;
    uint16_t test_r;
    if (win_init == 0)
    {
        diode_mode();
        win_buf = &onoff_Win;
        win_init = 1;
        clear_window();
    }
    uint16_t pen_val = get_pen_val();
    test_r = (pen_val / 2);
    //分段电阻补偿
    if(test_r <4)
    {
        test_r = 0;
    }
    else if (test_r < 85)
    {
        test_r += 6;
    }
    else if (test_r < 106)
    {
        test_r += 5;
    }
    else if (test_r < 157)
    {
        test_r += 4;
    }
    else if (test_r < 177)
    {
        test_r += 3;
    }
    for (int i = 0; i < win_buf->list_len; i++)
    {
        if (win_buf->menu_id == i)
        {
            fc = bc_color[i + 1];
            bc = BLACK;
            select_item_color(i + 1);
            if (win_buf->mode == 1)
            {
                num_flash(60, cursor_y + (i + 1) * FONT_H, win_buf->paramList[i].val, 4, fc, bc);
            }
        }
        else
        {
            fc = BLACK;
            bc = bc_color[i + 1];
        }
        LCD_ShowString(cursor_x, cursor_y + (i + 1) * FONT_H, win_buf->menuList[i].str, fc, bc, FONT_SIZE, 0);
        LCD_ShowIntNum(60, cursor_y + (i + 1) * FONT_H, win_buf->paramList[i].val, 4, fc, bc, FONT_SIZE);
        LCD_Show_array(130, cursor_y + FONT_H + 4, 16, 21, fc, bc, oumu);
    }
    // show_pen_volt(60, cursor_y, pen_val, BLACK, bc_color[0]);
    if (test_r > win_buf->paramList[win_buf->menu_id].max)
    {
        LCD_ShowString(60, cursor_y, "  OL", BLACK, bc_color[0], FONT_SIZE,0);
    }
    else
    {
        LCD_ShowIntNum(60, cursor_y, test_r, 4, BLACK, bc_color[0], FONT_SIZE);
    }
    show_bat_percent();
    if (win_buf->paramList[0].val > test_r)
    {
        set_led_green(0); // 亮
        set_fm_onoff(1);
    }
    else
    {
        set_led_green(1); // 灭
        set_fm_onoff(0);
    }
}
void onoff_control(uint32_t key_sta)
{
    if (win_init == 0)
    {
        win_buf = &onoff_Win;
    }
    switch (key_sta)
    {
    case KEY_UP:
        if (win_buf->mode == 0)
        {
            if (win_buf->menu_id > 0)
                win_buf->menu_id--;
        }
        else
        {
            if (win_buf->paramList[win_buf->menu_id].val <
                win_buf->paramList[win_buf->menu_id].max)
            {
                win_buf->paramList[win_buf->menu_id].val += win_buf->paramList[win_buf->menu_id].step;
            }
        }
        break;
    case KEY_DOW:
        if (win_buf->mode == 0)
        {
            win_buf->menu_id++;
            if (win_buf->menu_id >= win_buf->list_len)
            {
                win_buf->menu_id = 0;
            }
        }
        else
        {
            if (win_buf->paramList[win_buf->menu_id].val >
                win_buf->paramList[win_buf->menu_id].min)
            {
                win_buf->paramList[win_buf->menu_id].val -= win_buf->paramList[win_buf->menu_id].step;
            }
        }
        break;
    case KEY_LEFT:
        // 返回
        back_to_main();
        set_led_green(1); // 灭
        set_fm_onoff(0);
        break;
    case KEY_RIGHT:
        win_buf->mode = win_buf->mode > 0 ? 0 : 1;
        break;
    case KEY_OK:

        break;
    default:
        break;
    }
}
void diode_window(uint32_t key_sta)
{
    uint16_t bc;
    uint16_t fc = BLACK;
    if (win_init == 0)
    {
        diode_mode();
        win_buf = &diode_Win;
        win_init = 1;
        clear_window();
    }
    uint16_t pen_val = get_pen_val();
    if(pen_val > 2500)
    {
        pen_val -= 50;
    }
    for (int i = 0; i < win_buf->list_len; i++)
    {
        bc = bc_color[i + 1];
        LCD_ShowString(cursor_x, cursor_y + (i + 1) * FONT_H, win_buf->menuList[i].str, fc, bc, FONT_SIZE, 0);
        LCD_ShowIntNum(win_buf->menuList[0].len + FONT_W, cursor_y + (i + 1) * FONT_H, pen_val, 5, fc, bc, FONT_SIZE);
    }
    LCD_ShowString(134, cursor_y + FONT_H, "mV", BLACK, bc_color[1], FONT_SIZE, 0);
    show_bat_percent();
}
void diode_control(uint32_t key_sta)
{
    if (win_init == 0)
    {
        win_buf = &diode_Win;
    }
    switch (key_sta)
    {
    case KEY_LEFT:
        // 返回
        back_to_main();
        break;
    case KEY_RIGHT:
        break;
    case KEY_OK:

        break;
    default:
        break;
    }
}
void adj_window(uint32_t key_sta)
{
    uint8_t ret = 0;
    if (win_init == 0)
    {
        win_buf = &adj_Win;
        win_init = 1;
        logical_level_mode();
        clear_window();
        switch_auto_disable(1);
        set_multiple_xn(1);//先校准x1档
        clear_adc_ref_cnt();
    }
    LCD_ShowString(cursor_x, cursor_y, "calibration", BLACK, bc_color[0], FONT_SIZE, 0);
    LCD_ShowString(cursor_x, cursor_y + FONT_H, "wait", BLACK, bc_color[1], FONT_SIZE, 0);
    do
    {
        ret = adc_soft_Calibration(1);
    } while (ret == 0);
    set_multiple_xn(10); // 校准x10档
    clear_adc_ref_cnt();
    do
    {
        ret = adc_soft_Calibration(10);
    } while (ret == 0);
    switch_auto_disable(0);
    clear_adc_ref_cnt();
    save_adc_ref_val();
    back_to_main();
}
void adj_control(uint32_t key_sta)
{
    if (win_init == 0)
    {
        win_buf = &adj_Win;
    }
    switch (key_sta)
    {
    case KEY_UP:

        break;
    case KEY_DOW:

        break;
    case KEY_LEFT:
        // 返回
        break;
    case KEY_RIGHT:

        break;
    case KEY_OK:

        break;
    default:
        break;
    }
}
