#ifndef _GPIO_H_
#define _GPIO_H_
#include "main.h"

#define PW_PORT CW_GPIOF
#define PW_PIN GPIO_PIN_7

#define LED_PORT CW_GPIOA
#define LED_GREEN_PIN GPIO_PIN_10
#define LED_RED_PIN GPIO_PIN_11
#define LED_LIGHT_PIN GPIO_PIN_15

#define KEY_PORT CW_GPIOB
#define KEY1_PIN GPIO_PIN_3
#define KEY2_PIN GPIO_PIN_9
#define KEY3_PIN GPIO_PIN_7
#define KEY4_PIN GPIO_PIN_5
#define KEY5_PIN GPIO_PIN_6

#define ASW_PORT CW_GPIOB
#define ASW1_PIN GPIO_PIN_10
#define ASW2_PIN GPIO_PIN_11
#define ASW3_PIN GPIO_PIN_2
#define ASW4_PIN GPIO_PIN_0
#define CS_CT_PORT CW_GPIOB
#define CS_CT_PIN GPIO_PIN_1
#define CS_CT_HIGHT() CS_CT_PORT->BSRR = CS_CT_PIN
#define CS_CT_LOW() CS_CT_PORT->BRR = CS_CT_PIN

#define ASW1_HIGHT() ASW_PORT->BSRR = ASW1_PIN
#define ASW1_LOW() ASW_PORT->BRR = ASW1_PIN
#define ASW2_HIGHT() ASW_PORT->BSRR = ASW2_PIN
#define ASW2_LOW() ASW_PORT->BRR = ASW2_PIN
#define ASW3_HIGHT() ASW_PORT->BSRR = ASW3_PIN
#define ASW3_LOW() ASW_PORT->BRR = ASW3_PIN
#define ASW4_HIGHT() ASW_PORT->BSRR = ASW4_PIN
#define ASW4_LOW() ASW_PORT->BRR = ASW4_PIN

#define INOVR_PORT CW_GPIOB
#define INOVR_PIN GPIO_PIN_8
void gpio_config(void);
void set_led_red(uint8_t sta);
void set_led_green(uint8_t sta);
void set_led_light(uint8_t sta);
void toggle_led_light(void);
void shutdown(void);
uint8_t key_scan(void);
#endif
