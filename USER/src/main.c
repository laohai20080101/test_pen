/******************************************************************************/
/** \file main.c
 **
 ** A detailed description is available at
 ** @link Sample Group Some description @endlink
 **
 **   - 2021-03-12  1.0  xiebin First version for Device Driver Library of Module.
 **
 ******************************************************************************/
/*******************************************************************************
*
* 代码许可和免责信息
* 武汉芯源半导体有限公司授予您使用所有编程代码示例的非专属的版权许可，您可以由此
* 生成根据您的特定需要而定制的相似功能。根据不能被排除的任何法定保证，武汉芯源半
* 导体有限公司及其程序开发商和供应商对程序或技术支持（如果有）不提供任何明示或暗
* 含的保证或条件，包括但不限于暗含的有关适销性、适用于某种特定用途和非侵权的保证
* 或条件。
* 无论何种情形，武汉芯源半导体有限公司及其程序开发商或供应商均不对下列各项负责，
* 即使被告知其发生的可能性时，也是如此：数据的丢失或损坏；直接的、特别的、附带的
* 或间接的损害，或任何后果性经济损害；或利润、业务、收入、商誉或预期可节省金额的
* 损失。
* 某些司法辖区不允许对直接的、附带的或后果性的损害有任何的排除或限制，因此某些或
* 全部上述排除或限制可能并不适用于您。
*
*******************************************************************************/
/******************************************************************************
 * Include files
 ******************************************************************************/
#include "main.h"
#include "cw32f030_flash.h"
#include "FreeRTOS.h"
#include "task.h"
#include "gpio.h"
#include "gui.h"
#include "app.h"
#include "pwm.h"
#include "adc.h"
#include "lcd.h"
#include "logo.h"
#include "log.h"
/******************************************************************************
 * Local pre-processor symbols/macros ('#define')
 ******************************************************************************/
#define LED_GPIO_PORT CW_GPIOB
#define LED_GPIO_PINS GPIO_PIN_8 | GPIO_PIN_9

/******************************************************************************
 * Global variable definitions (declared in header file with 'extern')
 ******************************************************************************/

/******************************************************************************
 * Local type definitions ('typedef')
 ******************************************************************************/

/******************************************************************************
 * Local function prototypes ('static')
 ******************************************************************************/

/******************************************************************************
 * Local variable definitions ('static')                                      *
 ******************************************************************************/

/******************************************************************************
 * Local pre-processor symbols/macros ('#define')
 ******************************************************************************/

/*****************************************************************************
 * Function implementation - global ('extern') and local ('static')
 ******************************************************************************/
// uint16_t now_bat_val;
// uint16_t pen_val;
uint8_t key_val;
uint8_t key_msk = 0;
// const uint8_t tets[11840] = {0};
void Delay(uint16_t nCount);
void RCC_64M_Init(void)
{
    uint8_t res = 0U;

    RCC_AHBPeriphClk_Enable(RCC_AHB_PERIPH_FLASH, ENABLE); // 打开FLASH时钟

    RCC_HSI_Enable(RCC_HSIOSC_DIV3); // 设置HSI = HSIOSC/3 = 16M

    //============================================================================
    // 以下从HSI切换到PLL
    RCC_PLL_Enable(RCC_PLLSOURCE_HSI, 16000000, RCC_PLL_MUL_4); // 开启PLL，PLL源为HSI
    FLASH_SetLatency(FLASH_Latency_2);                          // 频率大于24M需要配置FlashWait=2
    res = RCC_SysClk_Switch(RCC_SYSCLKSRC_PLL);                 // 切换系统时钟到PLL
    if (res == 0x00)                                            // 切换系统时钟成功
    {
    }
}
void task_1(void *msg)
{
    while (1)
    {
        /* code */
        scan_shutdown();
        vTaskDelay(10);
    }
    
}
uint32_t window_time2;
uint32_t start_time2;
void task_2(void *msg)
{
    while (1)
    {
        /* code */
        wait_dma_complete();
        key_msk = key_scan();
        run_control(key_msk);
        vTaskDelay(10);
    }
}
uint32_t window_time;
uint32_t start_time;
void show_task(void *msg)
{
    while (1)
    {
        start_time = xTaskGetTickCount();
        run_window(0);
        window_time = xTaskGetTickCount() - start_time;
        vTaskDelay(10);
    }
}
void task_3(void *msg)
{
    while (1)
    {
        /* code */
        auto_multiple();
        vTaskDelay(1);
    }
}

char buf[200];
void task_sta(void *msg)
{
uint16_t bat;
    while (1)
    {
        /* code */
        // vTaskList(buf);
        // printf("%s\n",buf);
        bat = get_bat_val();
        // printf("samples:%u\n",bat);
        // printf("test\n");
        vTaskDelay(1000);
    }
}
/**
 ******************************************************************************
 ** \brief  Main function of project
 **
 ** \return uint32_t return value, if needed
 **
 ** LED1, LED2闪烁
 **
 ******************************************************************************/
int32_t main(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    RCC_64M_Init();
    gpio_config();
    gui_config();
    PWM_OutputConfig();
    adc_config();
    read_adc_ref_val();
    LogInit();
    // gImage_logo
    LCD_ShowPicture(0,0,160,80,gImage_logo);
    // LCD_Fill(0,57,160,80,BLACK);
    Delay(0xFFFF);
    set_led_green(1);//关绿灯
    //创建任务
    xTaskCreate(task_1,"task1",40,NULL,0,NULL);
    xTaskCreate(task_2,"task2",40,NULL,1,NULL);
    xTaskCreate(task_3, "task3", 60, NULL, 0, NULL);
    xTaskCreate(show_task,"show",200,NULL,2,NULL);
    xTaskCreate(task_sta,"task_sta",120,NULL,0,NULL);
    //开启任务调度
    vTaskStartScheduler();
    while (1)
    {

    }
}

/**
 * @brief 循环延时
 *
 * @param nCount
 */
void Delay(__IO uint16_t nCount)
{
    /* Decrement nCount value */
    while (nCount--)
    {
        for (int i = 0; i < 200; i++)
        {
            /* code */
        }
        
    }
}
void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
	( void ) pcTaskName;
	( void ) pxTask;

	/* Run time stack overflow checking is performed if
	configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
	function is called if a stack overflow is detected. */
	taskDISABLE_INTERRUPTS();
	for( ;; );
}
/******************************************************************************
 * EOF (not truncated)
 ******************************************************************************/
#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @return None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
       tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
