#include "app.h"
#include "gpio.h"
#include "adc.h"
#include "lcd_init.h"
uint8_t auto_disable = 0;
void set_multiple_xn(uint8_t xn)
{
    if (xn == 1)
    {
        pen_gears = 1;
        ASW2_HIGHT();
    }
    else if (xn == 10)
    {
        pen_gears = 10;
        ASW2_LOW();
    }
}
/**
 * @brief 
 * 
 * @param sta 1 --- 关闭自动档位
 */
void switch_auto_disable(uint8_t sta)
{
    auto_disable = sta;
}
/**
 * @brief 设置倍数
 * 
 * @return uint8_t 返回0是10x档
 */
void auto_multiple(void)
{
    uint16_t volt = pen_volt();
    static uint16_t cnt;
    if (auto_disable == 0)
    {
        if (pen_gears == 10)
        {
            // x10档是否切换x1
            if (volt < 240)
            {
                cnt++;
                if (cnt > 5)
                {
                    set_multiple_xn(1);
                    cnt = 0;
                }
            }
            else
            {
                cnt = 0;
            }
        }
        else
        {
            if (volt >= 2400)
            {
                cnt++;
                if (cnt > 5)
                {
                    // 10x
                    set_multiple_xn(10);
                    cnt = 0;
                }
            }
            else
            {
                cnt = 0;
            }
        }
    }
}
void logical_level_mode(void)
{
    ASW1_LOW();
    ASW3_LOW();
    ASW4_LOW();
    CS_CT_LOW();
}
void pwm_out_mode(void)
{
    ASW1_HIGHT();
    ASW3_HIGHT();
    ASW4_LOW();
    CS_CT_LOW();
}
void dc_out_mode(void)
{
    ASW1_LOW();
    ASW3_HIGHT();
    ASW4_LOW();
    CS_CT_LOW();
}
/**
 * @brief 通断/二极管
 *
 */
void diode_mode(void)
{
    ASW1_LOW();
    ASW3_LOW();
    ASW4_HIGHT();
    CS_CT_HIGHT();
}
void scan_shutdown(void)
{
    // static uint8_t sta = 0;
    static uint32_t cnt = 0;
    if(cnt > 10)
    {
        //息屏
        LCD_BLK_Clr();
        shutdown();
    }
    if (GPIO_ReadPin(KEY_PORT, KEY1_PIN) == GPIO_Pin_RESET)
    {
        cnt++;
    }
    else
    {
        cnt = 0;
    }
}
